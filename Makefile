CFLAGS = -Wall -g -o findtriangle
CC = gcc

all: findtriangle

findtriangle: main.c
	$(CC) $(CFLAGS) main.c

clean:
	rm findtriangle