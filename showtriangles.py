#!/usr/bin/env python
#/usr/bin/python

from matplotlib.pyplot import figure, show

#le coordenadas do ponto
[x,y] = raw_input().split(" ")

v_size = raw_input()

vertX = []
vertY = []

maiorX = 0
maiorY = 0
menorX = 999
menorY = 999

for i in range (int(v_size)):
	[a, b] = raw_input().split(" ")
	if int(a) > int(maiorX):
		maiorX = int(a)
	if int(b) > int(maiorY):
		maiorY = int(b)
	if int(a) < int(menorX):
		menorX = int(a)
	if int(b) < int(menorY):
		menorY = int(b)
	vertX.append(a)
	vertY.append(b)


#dimensoes da figura num retangulo de 8 por 8
fig = figure(1,figsize=(8,5))
#em xlim e ylim coloca-se os limites da coisa nos respect eixos
ax = fig.add_subplot(111, autoscale_on=False, xlim=((int(menorX)-1),(int(maiorX)+1)), ylim=((int(menorY)-1),(int(maiorY)+1)))

#escreve pontos na imagem
pontos, = ax.plot([x], [y], 'ro')

t_size = raw_input()

Triangs = []
bariX = []
bariY = []

for i in range (int(t_size)):
	[v1,v2,v3,t1,t2,t3] = raw_input().split(" ")
	line, = ax.plot([vertX[int(v1)-1], vertX[int(v2)-1], vertX[int(v3)-1], vertX[int(v1)-1]], [vertY[int(v1)-1], vertY[int(v2)-1], vertY[int(v3)-1], vertY[int(v1)-1]], lw=3, color='blue')
	bariX.append((int(vertX[int(v1)-1]) + int(vertX[int(v2)-1]) + int(vertX[int(v3)-1]))/3)
	bariY.append((int(vertY[int(v1)-1]) + int(vertY[int(v2)-1]) + int(vertY[int(v3)-1]))/3)
	ax.annotate(int(i)+1, xy=(bariX[-1], bariY[-1]),  xycoords='data', textcoords='offset points')
	Triangs.append([v1, v2, v3, t1, t2, t3])

listX = []
listY = []
c = []

while True:
	c = raw_input().split(" ")
	if EOFError:
		break

for i in c:
	listX.append(bariX[int(float(i))-1])
	listY.append(bariY[int(float(i))-1])

line, = ax.plot(listX, listY, lw=3, color='purple')

show()