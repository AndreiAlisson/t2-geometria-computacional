/*Autor: Andrei Alisson GRR20163061*/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

/*Estrutura de um veŕtice com suas coordenadas e índice*/
typedef struct Vertice{

	float coordX;	//Coordenada X dos veŕtices
	float coordY;	//Coordenada Y dos vértices
	int index;	//Índice do vértice na lista [1..N]

}Vert;

/*Contém o número e a lista de vértices*/
typedef struct ListVertices{

	int size;	//Número de vértices
	Vert *vert;	//Vértices

}VList;

/*Vetor de estrutura dos triângulos*/
typedef struct Triangulo{

	Vert *vertA, *vertB, *vertC;//Ponteiros para vértices
	int vizA, vizB, vizC;		//Índices dos triângulos vizinhos
	int index;					//Índice do triângulo na lista [1..N]
	int indexA, indexB, indexC;	//Índices dos vértices do triângulo
	int viz_control;			//Variável de controle para atribuição de vizinhos
	float bariX, bariY;			//Baricentro do triângulo

}Triang;

/*Contém o número e a lista de triângulos*/
typedef struct ListTriangulos{

	int size;		//Número de triângulos
	Triang *triang;	//Triângulos

}TList;

/*Pega o vizinho oposto ao vértice v*/
int get_Vizinho_Oposto(TList t, Triang tr, int index){

	if(tr.vizA > 0 && t.triang[tr.vizA-1].indexA != index && t.triang[tr.vizA-1].indexB != index && t.triang[tr.vizA-1].indexC != index){
		return tr.vizA;
	}
	else if(tr.vizB > 0 &&  t.triang[tr.vizB-1].indexA != index && t.triang[tr.vizB-1].indexB != index && t.triang[tr.vizB-1].indexC != index){
		return tr.vizB;
	}
	else if(tr.vizC > 0 &&  t.triang[tr.vizC-1].indexA != index && t.triang[tr.vizC-1].indexB != index && t.triang[tr.vizC-1].indexC != index){
		return tr.vizC;
	}
	else
		return 0;

}

/*Imprime saída especificada*/
void saida(float x, float y, VList v, TList t){

	printf("%.0f %.0f\n", x, y);
	printf("%d\n", v.size);
	for(int i = 0; i < v.size; i++)
		printf("%.0f %.0f\n", v.vert[i].coordX, v.vert[i].coordY);

	printf("%d\n", t.size);
	for(int i = 0; i < t.size; i++){
		printf("%d %d %d %d %d %d\n",
			t.triang[i].indexA, t.triang[i].indexB, t.triang[i].indexC,
			get_Vizinho_Oposto(t, t.triang[i], t.triang[i].indexA),
			get_Vizinho_Oposto(t, t.triang[i], t.triang[i].indexB),
			get_Vizinho_Oposto(t, t.triang[i], t.triang[i].indexC));
			//t.triang[i].vizA, t.triang[i].vizB, t.triang[i].vizC);
	}
}


/*Obtem a lista de vizinhos dos triângulos*/
void get_Vizinhos(TList *t){

	int v_inter = 0;

	for(int i = 0; i < t->size - 1; i++){
		for(int j = i + 1; j < t->size; j++){

			if(t->triang[i].indexA == t->triang[j].indexA)
				v_inter++;
			else if(t->triang[i].indexA == t->triang[j].indexB)
				v_inter++;
			else if(t->triang[i].indexA == t->triang[j].indexC)
				v_inter++;

			if(t->triang[i].indexB == t->triang[j].indexA)
				v_inter++;
			else if(t->triang[i].indexB == t->triang[j].indexB)
				v_inter++;
			else if(t->triang[i].indexB == t->triang[j].indexC)
				v_inter++;

			if(t->triang[i].indexC == t->triang[j].indexA)
				v_inter++;
			else if(t->triang[i].indexC == t->triang[j].indexB)
				v_inter++;
			else if(t->triang[i].indexC == t->triang[j].indexC)
				v_inter++;

			if(v_inter == 2){

				if(t->triang[i].viz_control == 0)
					t->triang[i].vizA = t->triang[j].index;		
				else if(t->triang[i].viz_control == 1)
					t->triang[i].vizB = t->triang[j].index;	
				else if(t->triang[i].viz_control == 2)
					t->triang[i].vizC = t->triang[j].index;	

				if(t->triang[j].viz_control == 0)					
					t->triang[j].vizA = t->triang[i].index;	
				else if(t->triang[j].viz_control == 1)				
					t->triang[j].vizB = t->triang[i].index;
				else if(t->triang[j].viz_control == 2)
					t->triang[j].vizC = t->triang[i].index;

				t->triang[i].viz_control++;
				t->triang[j].viz_control++;
				
			}

			v_inter = 0;

		}

	}

}

/*Calcula o baricentro dos triângulos*/
void get_Baricentro(TList *t){

	for(int i = 0; i < t->size; i++){

		t->triang[i].bariX = (t->triang[i].vertA->coordX +
							  t->triang[i].vertB->coordX +
						   	  t->triang[i].vertC->coordX) / 3.0;
		t->triang[i].bariY = (t->triang[i].vertA->coordY +
						   	  t->triang[i].vertB->coordY +
						  	  t->triang[i].vertC->coordY) / 3.0;

	}

}

int tem_Intersec(float x1, float y1, float x2, float y2, float x3, float y3, float x4, float y4){

    double det, s, t;
    double px, py;
    double epsilon = 0.00001;
    px = py = 0.0;

    det = (x4 - x3) * (y2 - y1)  -  (y4 - y3) * (x2 - x1);

    //0 -> não há intersecção
    if(det == 0.0)
        return 0;

    s = ((x4 - x3) * (y3 - y1) - (y4 - y3) * (x3 - x1))/ det;
    t = ((x2 - x1) * (y3 - y1) - (y2 - y1) * (x3 - x1))/ det;

    px = x1 + (x2 - x1)*s;
    py = y1 + (y2 - y1)*s;

    if(s < 1.0 && s > 0.0 && t < 1.0 && t > 0.0){
        return 1;
    }
    else if((fabs(px - x3) < epsilon && fabs(py - y3) < epsilon) || (fabs(px - x4) < epsilon && fabs(py - y4) < epsilon)){
    	return 2;
    }
    else{
        return 0;
    }
}

/*Procura o vizinho da aresta aresta no triângulo index, onde 1 = ab, 2 = bc, 3 = ca*/
int find_Vizinho(TList t, int index, int aresta){

	int vizinhos = 0;

	//aresta ab
	if(aresta == 1){

		//vizA
		if(t.triang[index].vizA > 0){
			//Procura aresta a com vizA
			if(t.triang[index].indexA == t.triang[t.triang[index].vizA-1].indexA)
				vizinhos++;
			else if(t.triang[index].indexA == t.triang[t.triang[index].vizA-1].indexB)
				vizinhos++;
			else if(t.triang[index].indexA == t.triang[t.triang[index].vizA-1].indexC)
				vizinhos++;

			//Procura aresta b com vizA
			if(t.triang[index].indexB == t.triang[t.triang[index].vizA-1].indexA)
				vizinhos++;
			else if(t.triang[index].indexB == t.triang[t.triang[index].vizA-1].indexB)
				vizinhos++;
			else if(t.triang[index].indexB == t.triang[t.triang[index].vizA-1].indexC)
				vizinhos++;

			if(vizinhos == 2)
				return t.triang[index].vizA;
			else
				vizinhos = 0;
		}

		//vizB
		if(t.triang[index].vizB > 0){
			//Procura aresta a com vizB
			if(t.triang[index].indexA == t.triang[t.triang[index].vizB-1].indexA)
				vizinhos++;
			else if(t.triang[index].indexA == t.triang[t.triang[index].vizB-1].indexB)
				vizinhos++;
			else if(t.triang[index].indexA == t.triang[t.triang[index].vizB-1].indexC)
				vizinhos++;

			//Procura aresta b com vizB
			if(t.triang[index].indexB == t.triang[t.triang[index].vizB-1].indexA)
				vizinhos++;
			else if(t.triang[index].indexB == t.triang[t.triang[index].vizB-1].indexB)
				vizinhos++;
			else if(t.triang[index].indexB == t.triang[t.triang[index].vizB-1].indexC)
				vizinhos++;

			if(vizinhos == 2)
				return t.triang[index].vizB;
			else
				vizinhos = 0;
		}

		//vizC
		if(t.triang[index].vizC > 0){
			//Procura aresta a com vizC
			if(t.triang[index].indexA == t.triang[t.triang[index].vizC-1].indexA)
				vizinhos++;
			else if(t.triang[index].indexA == t.triang[t.triang[index].vizC-1].indexB)
				vizinhos++;
			else if(t.triang[index].indexA == t.triang[t.triang[index].vizC-1].indexC)
				vizinhos++;

			//Procura aresta b com vizC
			if(t.triang[index].indexB == t.triang[t.triang[index].vizC-1].indexA)
				vizinhos++;
			else if(t.triang[index].indexB == t.triang[t.triang[index].vizC-1].indexB)
				vizinhos++;
			else if(t.triang[index].indexB == t.triang[t.triang[index].vizC-1].indexC)
				vizinhos++;

			if(vizinhos == 2)
				return t.triang[index].vizC;
		}

		return -1;	//Não encontrou, erro.
		
	}
	//aresta bc
	else if(aresta == 2){

		//vizA
		if(t.triang[index].vizA > 0){
			//Procura aresta b com vizA
			if(t.triang[index].indexB == t.triang[t.triang[index].vizA-1].indexA)
				vizinhos++;
			else if(t.triang[index].indexB == t.triang[t.triang[index].vizA-1].indexB)
				vizinhos++;
			else if(t.triang[index].indexB == t.triang[t.triang[index].vizA-1].indexC)
				vizinhos++;

			//Procura aresta c com vizA
			if(t.triang[index].indexC == t.triang[t.triang[index].vizA-1].indexA)
				vizinhos++;
			else if(t.triang[index].indexC == t.triang[t.triang[index].vizA-1].indexB)
				vizinhos++;
			else if(t.triang[index].indexC == t.triang[t.triang[index].vizA-1].indexC)
				vizinhos++;

			if(vizinhos == 2)
				return t.triang[index].vizA;
			else
				vizinhos = 0;
		}

		//vizB
		if(t.triang[index].vizB > 0){
			//Procura aresta a com vizB
			if(t.triang[index].indexB == t.triang[t.triang[index].vizB-1].indexA)
				vizinhos++;
			else if(t.triang[index].indexB == t.triang[t.triang[index].vizB-1].indexB)
				vizinhos++;
			else if(t.triang[index].indexB == t.triang[t.triang[index].vizB-1].indexC)
				vizinhos++;

			//Procura aresta b com vizB
			if(t.triang[index].indexC == t.triang[t.triang[index].vizB-1].indexA)
				vizinhos++;
			else if(t.triang[index].indexC == t.triang[t.triang[index].vizB-1].indexB)
				vizinhos++;
			else if(t.triang[index].indexC == t.triang[t.triang[index].vizB-1].indexC)
				vizinhos++;

			if(vizinhos == 2)
				return t.triang[index].vizB;
			else
				vizinhos = 0;
		}

		//vizC
		if(t.triang[index].vizC > 0){
			//Procura aresta a com vizC
			if(t.triang[index].indexB == t.triang[t.triang[index].vizC-1].indexA)
				vizinhos++;
			else if(t.triang[index].indexB == t.triang[t.triang[index].vizC-1].indexB)
				vizinhos++;
			else if(t.triang[index].indexB == t.triang[t.triang[index].vizC-1].indexC)
				vizinhos++;

			//Procura aresta b com vizC
			if(t.triang[index].indexC == t.triang[t.triang[index].vizC-1].indexA)
				vizinhos++;
			else if(t.triang[index].indexC == t.triang[t.triang[index].vizC-1].indexB)
				vizinhos++;
			else if(t.triang[index].indexC == t.triang[t.triang[index].vizC-1].indexC)
				vizinhos++;

			if(vizinhos == 2)
				return t.triang[index].vizC;
		}

		return -1;	//Não encontrou, erro.

	}
	//aresta ca
	else if(aresta == 3){

		//vizA
		if(t.triang[index].vizA > 0){
			//Procura aresta c com vizA
			if(t.triang[index].indexC == t.triang[t.triang[index].vizA-1].indexA)
				vizinhos++;
			else if(t.triang[index].indexC == t.triang[t.triang[index].vizA-1].indexB)
				vizinhos++;
			else if(t.triang[index].indexC == t.triang[t.triang[index].vizA-1].indexC)
				vizinhos++;

			//Procura aresta a com vizA
			if(t.triang[index].indexA == t.triang[t.triang[index].vizA-1].indexA)
				vizinhos++;
			else if(t.triang[index].indexA == t.triang[t.triang[index].vizA-1].indexB)
				vizinhos++;
			else if(t.triang[index].indexA == t.triang[t.triang[index].vizA-1].indexC)
				vizinhos++;

			if(vizinhos == 2)
				return t.triang[index].vizA;
			else
				vizinhos = 0;
		}

		//vizB
		if(t.triang[index].vizB > 0){
			//Procura aresta c com vizB
			if(t.triang[index].indexC == t.triang[t.triang[index].vizB-1].indexA)
				vizinhos++;
			else if(t.triang[index].indexC == t.triang[t.triang[index].vizB-1].indexB)
				vizinhos++;
			else if(t.triang[index].indexC == t.triang[t.triang[index].vizB-1].indexC)
				vizinhos++;

			//Procura aresta a com vizB
			if(t.triang[index].indexA == t.triang[t.triang[index].vizB-1].indexA)
				vizinhos++;
			else if(t.triang[index].indexA == t.triang[t.triang[index].vizB-1].indexB)
				vizinhos++;
			else if(t.triang[index].indexA == t.triang[t.triang[index].vizB-1].indexC)
				vizinhos++;

			if(vizinhos == 2)
				return t.triang[index].vizB;
			else
				vizinhos = 0;
		}

		//vizC
		if(t.triang[index].vizC > 0){
			//Procura aresta c com vizC
			if(t.triang[index].indexC == t.triang[t.triang[index].vizC-1].indexA)
				vizinhos++;
			else if(t.triang[index].indexC == t.triang[t.triang[index].vizC-1].indexB)
				vizinhos++;
			else if(t.triang[index].indexC == t.triang[t.triang[index].vizC-1].indexC)
				vizinhos++;

			//Procura aresta a com vizC
			if(t.triang[index].indexA == t.triang[t.triang[index].vizC-1].indexA)
				vizinhos++;
			else if(t.triang[index].indexA == t.triang[t.triang[index].vizC-1].indexB)
				vizinhos++;
			else if(t.triang[index].indexA == t.triang[t.triang[index].vizC-1].indexC)
				vizinhos++;

			if(vizinhos == 2)
				return t.triang[index].vizC;
		}

	}

	return -1; //Não encontrou, erro.

}

/*Verifica se o ponto está contido em um triângulo t*/
int found_Triangle(Triang t, float x, float y){

	float v0X, v0Y, v1X, v1Y, v2X, v2Y;
	float dot00, dot01, dot02, dot11, dot12;
	float invDenom;
	float u, v;

	v0X = t.vertC->coordX - t.vertA->coordX;
	v0Y = t.vertC->coordY - t.vertA->coordY;
	v1X = t.vertB->coordX - t.vertA->coordX;
	v1Y = t.vertB->coordY - t.vertA->coordY;
	v2X = x - t.vertA->coordX;
	v2Y = y - t.vertA->coordY;

	dot00 = v0X * v0X + v0Y * v0Y;
	dot01 = v0X * v1X + v0Y * v1Y;
	dot02 = v0X * v2X + v0Y * v2Y;
	dot11 = v1X * v1X + v1Y * v1Y;
	dot12 = v1X * v2X + v1Y * v2Y;

	invDenom = (dot00 * dot11 - dot01 * dot01);
	u = (dot11 * dot02 - dot01 * dot12) / invDenom;
	v = (dot00 * dot12 - dot01 * dot02) / invDenom;

	if((u >= 0) && (v >= 0) && (u + v < 1))
		return 1;
	return 0;

}

/*procura em qual triângulo está o ponto (x,y) nos triãngulos t a partir do triângulo index*/
int find_Ponto(float x, float y, TList t, int index){
	
	int ret[3];
	//Limitador de execução
	for(int i = 0; i < t.size; i++){

		if(found_Triangle(t.triang[index-1], x, y)){
			printf("%d", index);
			return index;
		}

		if((ret[0] = tem_Intersec(t.triang[index-1].bariX, t.triang[index-1].bariY,
						x, y,
						t.triang[index-1].vertA->coordX, t.triang[index-1].vertA->coordY,
						t.triang[index-1].vertB->coordX, t.triang[index-1].vertB->coordY)) == 1){

			printf("%d ", index);
			index = find_Vizinho(t, index-1, 1);
			

		}
		else if((ret[1] = tem_Intersec(t.triang[index-1].bariX, t.triang[index-1].bariY,
						x, y,
						t.triang[index-1].vertB->coordX, t.triang[index-1].vertB->coordY,
						t.triang[index-1].vertC->coordX, t.triang[index-1].vertC->coordY)) == 1){

			printf("%d ", index);
			index = find_Vizinho(t, index-1, 2);

		}
		else if((ret[2] = tem_Intersec(t.triang[index-1].bariX, t.triang[index-1].bariY,
						x, y,
						t.triang[index-1].vertC->coordX, t.triang[index-1].vertC->coordY,
						t.triang[index-1].vertA->coordX, t.triang[index-1].vertA->coordY)) == 1){

			printf("%d ", index);
			index = find_Vizinho(t, index-1, 3);

		}
		else if(ret[0] + ret[1] + ret[2] > 0){
			printf("%d ", index);
			if(ret[0] == 2)
				index = find_Vizinho(t, index-1, 1);
			else if(ret[1] == 2)
				index = find_Vizinho(t, index-1, 2);
			else if(ret[2] == 2)
				index = find_Vizinho(t, index-1, 3);
		}
		else{
			printf("%d", index);
			return index;
		}

	}

	return -1;

}

int main(){

	VList v;
	TList t;
	float x, y;
	int numVerts;
	int numTriangs;

	int tempA, tempB, tempC;

	scanf("%f %f", &x, &y);
	scanf("%d", &numVerts);

	v.vert = malloc(numVerts * sizeof(Vert));
	v.size = numVerts;

	for(int i = 0; i < numVerts; i++){
		scanf("%f %f", &v.vert[i].coordX, &v.vert[i].coordY);
		v.vert[i].index = i+1;
	}

	scanf("%d", &numTriangs);

	t.triang = malloc(numTriangs * sizeof(Triang));
	t.size = numTriangs;

	for(int i = 0; i < numTriangs; i++){
		scanf("%d %d %d", &tempA, &tempB, &tempC);
		t.triang[i].vertA = &v.vert[tempA-1];
		t.triang[i].vertB = &v.vert[tempB-1];
		t.triang[i].vertC = &v.vert[tempC-1];
		t.triang[i].index = i+1;
		t.triang[i].indexA = tempA;
		t.triang[i].indexB = tempB;
		t.triang[i].indexC = tempC;
		t.triang[i].viz_control = 0;
	}
	
	get_Vizinhos(&t);

	get_Baricentro(&t);

	saida(x, y, v, t);

	if(find_Ponto(x, y, t, 1) == -1)
		fprintf(stderr, "Erro, ultrapassou limite de iterações\n");

	free(v.vert);
	free(t.triang);

}